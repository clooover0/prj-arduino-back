import mongoose from "mongoose";

const profileSchema = mongoose.Schema({
    name: {
        type: String,
        required: true,
        trim: true,
    },
    sessions: [
        {
            type: mongoose.Schema.Types.ObjectId,
            ref: "ProfileSession",
        },
    ],
    user: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "User",
    },
    createdAt: {
        type: Date,
        default: new Date(),
    },
});

const Profile = mongoose.model("Profile", profileSchema);
export default Profile;
