import mongoose from "mongoose";

const profileSessionSchema = mongoose.Schema({
    datas: [
        {
            type: mongoose.Schema.Types.ObjectId,
            ref: "ProfileData",
        },
    ],

    profile: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "Profile",
    },
    createdAt: {
        type: Date,
        default: new Date(),
    },
});

const ProfileSession = mongoose.model("ProfileSession", profileSessionSchema);
export default ProfileSession;
