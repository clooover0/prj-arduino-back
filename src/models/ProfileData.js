import mongoose from "mongoose";

const profileDataSchema = mongoose.Schema({
    value: {
        type: Number,
        required: true,
    },

    profileSession: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "ProfileSession",
    },
    createdAt: {
        type: Date,
        default: new Date(),
    },
});

const ProfileData = mongoose.model("ProfileData", profileDataSchema);
export default ProfileData;
