import express from "express";
import Profile from "../models/Profile.js";
import auth from "../middleware/auth.js";
import ProfileSession from "../models/ProfileSession.js";
import ProfileData from "../models/ProfileData.js";
// consts
const router = express.Router();

router.post("/profiles", auth, async (req, res) => {
    const { name } = req.body;
    try {
        const profile = new Profile({
            user: req.user._id,
            name,
        });

        await profile.save();

        const user = req.user;

        user.profiles.push(profile);
        await user.save();

        res.status(201).send(profile);
    } catch (error) {
        res.status(400).send(error);
        console.log(error);
    }
});

router.get("/profiles", auth, async (req, res) => {
    const profiles = await Profile.find({ user: req.user._id });

    for (const profile of profiles) {
        const sessions = await ProfileSession.find({ profile: profile._id });

        for (const session of sessions) {
            const datas = await ProfileData.find({
                profileSession: session._id,
            });
            session.datas = datas;
        }
        profile.sessions = sessions;
    }

    res.json(profiles);
});

router.post("/profiles/:id/sessions", auth, async (req, res) => {
    const profileId = req.params.id;
    const profile = await Profile.findOne({ _id: profileId });
    const { datas } = req.body;

    const profileDatas = datas.map(
        data => new ProfileData({ value: data.value })
    );
    const profileSession = new ProfileSession({
        profile: profileId,
    });

    await profileSession.save();

    for (const profileData of profileDatas) {
        profileData.profileSession = profileSession._id;
        await profileData.save();
    }

    profile.profileSession = profileSession._id;

    await profile.save();

    res.json(profile);
});

export default router;
