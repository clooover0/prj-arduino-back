import cors from "cors";
import express from "express";
import "./db/db.js";
import profileRouter from "./routers/profile.js";
import userRouter from "./routers/user.js";

const port = process.env.PORT;

const app = express();

app.use(express.json());
app.use(cors());
app.use(userRouter);
app.use(profileRouter);

app.listen(port, () => {
    console.log(`Server running on port ${port}`);
});
